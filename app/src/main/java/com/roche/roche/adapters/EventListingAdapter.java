package com.roche.roche.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.roche.roche.R;


/**
 * Created by hshaikh on 6/28/2016.
 */
public class EventListingAdapter extends ArrayAdapter<String> {

    private String[] names;
    private String[] desc;
    private Integer[] imageid;
    private Activity context;


    public EventListingAdapter(Activity context, String[] names, String[] desc) {
        super(context, R.layout.listview_message_layout, names);
        this.context = context;
        this.names = names;
        this.desc = desc;

     //   this.imageid = imageid;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView==null)
        {
            LayoutInflater inflater = context.getLayoutInflater();
            convertView=inflater.inflate(R.layout.my_upcoming_event_item,parent,false);
        }
        if(position %2 == 1)
        {
            // Set a background color for ListView regular row/item
            convertView.setBackgroundColor(Color.parseColor("#eceff4"));
        }
        else
        {
            // Set the background color for alternate row/item
            convertView.setBackgroundColor(Color.parseColor("#ffffff"));
        }
     //   categories gm = (categories)getItem(position);
        //  ImageView img = (ImageView) convertView.findViewById(R.id.imageView1);
        //   TextView txt = (TextView) convertView.findViewById(R.id.textView1);
        //   TextView txt2 = (TextView) convertView.findViewById(R.id.textView2);
      //  categories grid = (categories) getItem(position);

        //  img.setImageResource(grid.getImage());
        //  txt.setText(grid.getCategoryname());
        return convertView;
    }

/*    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.event_listing_layout, null, true);
        // TextView textViewName = (TextView) listViewItem.findViewById(R.id.textViewName);
        //  TextView textViewDesc = (TextView) listViewItem.findViewById(R.id.textViewDesc);
        // ImageView image = (ImageView) listViewItem.findViewById(R.id.imageView);

        // textViewName.setText(names[position]);
        //  textViewDesc.setText(desc[position]);
        //  image.setImageResource(imageid[position]);

        if(position %2 == 1)
        {
            // Set a background color for ListView regular row/item
            convertView.setBackgroundColor(Color.parseColor("#eceff4"));
        }
        else
        {
            // Set the background color for alternate row/item
            convertView.setBackgroundColor(Color.parseColor("#ffffff"));
        }

        return  listViewItem;
    }*/
}
