package com.roche.roche.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.roche.roche.R;

/**
 * Created by hshaikh on 6/23/2016.
 */

public class MyTeamListview  extends ArrayAdapter<String> {

    private String[] names;
    private String[] desc;
    private String[] imageid;
    private Activity context;

    public MyTeamListview(Activity context, String[] names) {
        super(context, R.layout.listview_layout_myteam, names);
        this.context = context;
        this.names = names;
        this.desc = desc;
        this.imageid = imageid;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.listview_layout_myteam, null, true);
      //  TextView textViewName = (TextView) listViewItem.findViewById(R.id.textViewName);
      //  TextView textViewDesc = (TextView) listViewItem.findViewById(R.id.textViewDesc);
      //  TextView image = (TextView) listViewItem.findViewById(R.id.imageView);

      //  textViewName.setText(names[position]);
      //  textViewDesc.setText(desc[position]);
       // image.setText(imageid[position]);
        return  listViewItem;
    }
}
