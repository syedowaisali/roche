package com.roche.roche.adapters;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.roche.roche.ui.fragments.HomeUpComingEventFragment;
import com.viewpagerindicator.IconPagerAdapter;

/**
 * Created by hshaikh on 6/26/2016.
 */
public class HomeUpComingEventsAdapter extends FragmentPagerAdapter implements IconPagerAdapter {
    protected static final String[] CONTENT = new String[] { "Welcome", "To", "WOM", "Project", "PageIndicator"};
//    protected static final int[] ICONS = new int[] {
//            R.drawable.perm_group_calendar,
//            R.drawable.perm_group_camera,
//            R.drawable.perm_group_device_alarms,
//            R.drawable.perm_group_location
//    };

    private int mCount = CONTENT.length;

    public HomeUpComingEventsAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Log.e("position adaptor", position + "");
//    		return PageIndicatorFragment.newInstance(CONTENT[position % CONTENT.length]);
        return new HomeUpComingEventFragment().newInstance(position);
    }

    @Override
    public int getCount() {
        return mCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return HomeUpComingEventsAdapter.CONTENT[position % CONTENT.length];
    }

    @Override
    public int getIconResId(int index) {
//      return ICONS[index % ICONS.length];
        return index;
    }

    public void setCount(int count) {
        if (count > 0 && count <= 10) {
            mCount = count;
            notifyDataSetChanged();
        }
    }
}
