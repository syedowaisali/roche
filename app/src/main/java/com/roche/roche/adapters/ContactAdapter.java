package com.roche.roche.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.roche.roche.R;
import com.roche.roche.models.categories;

import java.util.ArrayList;

/**
 * Created by hshaikh on 7/20/2016.
 */
public class ContactAdapter extends ArrayAdapter<categories> {


    Context contxt;
    ArrayList<categories> list;
    LayoutInflater layout;
    private TabLayout tabLayout;

    public ContactAdapter(Context contxt, ArrayList<categories> list2) {
        super(contxt, 0, list2);
        this.contxt = contxt;
        this.list = list2;
        layout = (LayoutInflater) contxt.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }




    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView==null)
        {
            convertView=layout.inflate(R.layout.contact_layout,parent,false);
        }
        categories gm = (categories)getItem(position);
        //  ImageView img = (ImageView) convertView.findViewById(R.id.imageView1);
        //   TextView txt = (TextView) convertView.findViewById(R.id.textView1);
        //   TextView txt2 = (TextView) convertView.findViewById(R.id.textView2);
        categories grid = (categories) getItem(position);
        if(position %2 == 1)
        {
            // Set a background color for ListView regular row/item
            convertView.setBackgroundColor(Color.parseColor("#eceff4"));
        }
        else
        {
            // Set the background color for alternate row/item
            convertView.setBackgroundColor(Color.parseColor("#ffffff"));
        }

        //  img.setImageResource(grid.getImage());
        //  txt.setText(grid.getCategoryname());
        return convertView;
    }
}
