package com.roche.roche.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.roche.roche.R;

/**
 * Created by hshaikh on 6/23/2016.
 */
public class CourseListAdapter extends ArrayAdapter<String> {


    private String[] names;
    private String[] desc;
    private Integer[] imageid;
    private Activity context;

    public CourseListAdapter(Activity context, String[] names, String[] desc, Integer[] imageid) {
        super(context, R.layout.listviewcouressinprogress, names);
        this.context = context;
        this.names = names;
        this.desc = desc;
        this.imageid = imageid;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.listviewcouressinprogress, null, true);
       // TextView textViewName = (TextView) listViewItem.findViewById(R.id.textViewName);
      //  TextView textViewDesc = (TextView) listViewItem.findViewById(R.id.textViewDesc);
       // ImageView image = (ImageView) listViewItem.findViewById(R.id.imageView);

       // textViewName.setText(names[position]);
      //  textViewDesc.setText(desc[position]);
      //  image.setImageResource(imageid[position]);


        if(position %2 == 1)
        {
            // Set a background color for ListView regular row/item
            listViewItem.setBackgroundColor(Color.parseColor("#eceff4"));
        }
        else
        {
            // Set the background color for alternate row/item
            listViewItem.setBackgroundColor(Color.parseColor("#ffffff"));
        }

        return  listViewItem;
    }
}
