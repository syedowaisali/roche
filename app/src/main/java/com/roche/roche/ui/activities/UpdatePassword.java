package com.roche.roche.ui.activities;

import android.os.Bundle;
import android.view.View;

import com.roche.roche.R;
import com.roche.roche.base.PageHeader;

public class UpdatePassword extends PageHeader {

    @Override
    public int getLayout() {
        return R.layout.activity_update_password;
    }

    @Override
    protected String getActionBarTitle() {
        return "Update Password";
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);

        // attach click listeners
        attachClickListener(R.id.btn_update_password);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.btn_update_password: break;
        }
    }
}
