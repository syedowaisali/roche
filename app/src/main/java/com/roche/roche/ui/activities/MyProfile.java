package com.roche.roche.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.roche.roche.R;
import com.roche.roche.base.PageHeader;


public class MyProfile extends PageHeader {

    Spinner spnr;

    String[] celebrities = {
            "Lab Manager",
            "Lab Manager",
            "Jessica Alba",
            "Brad Pitt",
            "Tom Cruise",
            "Johnny Depp",
            "Megan Fox",
            "Paul Walker",
            "Vin Diesel"
    };

    @Override
    public int getLayout() {
        return R.layout.activity_my_profile;
    }

    @Override
    protected String getActionBarTitle() {
        return "Profile Setting";
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);

        spnr = (Spinner)findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.register_drpdown_text, celebrities);
        adapter.setDropDownViewResource(R.layout.register_dromdown_text_value);

        spnr.setAdapter(adapter);
        spnr.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> arg0, View arg1,
                                               int arg2, long arg3) {

                        int position = spnr.getSelectedItemPosition();
                        //Toast.makeText(getApplicationContext(),"You have selected "+celebrities[+position], Toast.LENGTH_LONG).show();
                        // TODO Auto-generated method stub
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub

                    }

                }
        );
    }

    public void pro(View v){
        Intent intent = new Intent(this,MyCertificates.class);
        startActivity(intent);
    }
}
