package com.roche.roche.ui.activities;

import android.os.Bundle;

import com.roche.roche.R;
import com.roche.roche.base.PageHeader;

public class SuggestCourse extends PageHeader {

    @Override
    public int getLayout() {
        return R.layout.activity_suggest_course;
    }

    @Override
    protected String getActionBarTitle() {
        return "Suggest Course";
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);
    }
}
