package com.roche.roche.ui.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.roche.roche.R;
import com.roche.roche.base.PageHeader;
import com.roche.roche.adapters.MyTeamListview;

public class MyTeam extends PageHeader {

    private ListView listView;
    private String names[] = {
            "HTML",
            "CSS",
            "Java Script",
            "Java Script",
            "Java Script",
            "Java Script",
            "Java Script",
            "Wordpress"
    };

    @Override
    public int getLayout() {
        return R.layout.activity_my_team;
    }

    @Override
    protected String getActionBarTitle() {
        return "My Team";
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);

        MyTeamListview customList = new MyTeamListview(this, names);

        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(customList);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
               // Toast.makeText(getApplicationContext(),"You Clicked "+names[i], Toast.LENGTH_SHORT).show();
            }
        });
    }
}
