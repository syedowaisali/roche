package com.roche.roche.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.flyco.pageindicator.indicator.base.PageIndicator;
import com.roche.roche.R;
import com.roche.roche.adapters.PageIndicatorAdaptor;


public class Tutorial extends FragmentActivity {
    PageIndicatorAdaptor mAdapter;
    ViewPager mPager;
    PageIndicator mIndicator;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        mAdapter = new PageIndicatorAdaptor(getSupportFragmentManager());
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);

        mIndicator = (PageIndicator) findViewById(R.id.indicator);
        mIndicator.setViewPager(mPager);
    }

    public void skip(View view){
        Intent intent = new Intent(this,SignIn.class);
        startActivity(intent);
    }
}
