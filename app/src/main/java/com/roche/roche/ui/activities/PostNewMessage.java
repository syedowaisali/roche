package com.roche.roche.ui.activities;

import android.os.Bundle;

import com.roche.roche.R;
import com.roche.roche.base.PageHeader;

public class PostNewMessage extends PageHeader {

    @Override
    public int getLayout() {
        return R.layout.activity_post_new_message;
    }

    @Override
    protected String getActionBarTitle() {
        return "Post New Message";
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);
    }
}
