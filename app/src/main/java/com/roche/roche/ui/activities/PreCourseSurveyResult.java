package com.roche.roche.ui.activities;

import android.os.Bundle;

import com.roche.roche.R;
import com.roche.roche.base.PageHeader;

public class PreCourseSurveyResult extends PageHeader {

    @Override
    public int getLayout() {
        return R.layout.activity_pre_course_survey_result;
    }

    @Override
    protected String getActionBarTitle() {
        return "Pre Course Survey";
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);
    }
}
