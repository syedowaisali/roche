package com.roche.roche.ui.activities;

import com.roche.roche.R;
import com.roche.roche.base.PageHeader;


public class PrivacyPolicy extends PageHeader {

    @Override
    public int getLayout() {
        return R.layout.activity_privacy_policy;
    }

    @Override
    protected String getActionBarTitle() {
        return "Privacy Policy";
    }
}
