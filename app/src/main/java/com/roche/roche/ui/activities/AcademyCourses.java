package com.roche.roche.ui.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.roche.roche.R;
import com.roche.roche.adapters.CourseListAdapter;
import com.roche.roche.base.PageHeader;

public class AcademyCourses extends PageHeader {

    private ListView listView;
    private String names[] = {
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd"

    };

    private String desc[] = {
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd"

    };

    private Integer imageid[] = {

    };

    @Override
    public int getLayout() {
        return R.layout.activity_academy_courses;
    }

    @Override
    protected String getActionBarTitle() {
        return "Academy Courses";
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);

        // attach click listener
        attachClickListener(R.id.btn_calendar, R.id.btn_filters);

        CourseListAdapter customList = new CourseListAdapter(this, names, desc, imageid);

        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(customList);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //Toast.makeText(getApplicationContext(),"You Clicked "+names[i], Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.btn_calendar: break;
            case R.id.btn_filters: gotoActivity(Filters.class); break;
        }
    }
}

