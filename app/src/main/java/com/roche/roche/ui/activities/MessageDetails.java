package com.roche.roche.ui.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.roche.roche.R;
import com.roche.roche.adapters.MessageDetailAdapter;
import com.roche.roche.base.PageHeader;

public class MessageDetails extends PageHeader {

    private ListView listView;
    private String names[] = {
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd"

    };

    private String desc[] = {
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd"

    };

    @Override
    public int getLayout() {
        return R.layout.activity_message_details;
    }

    @Override
    protected String getActionBarTitle() {
        return "Message Detail";
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);


        MessageDetailAdapter customListview = new MessageDetailAdapter(this, names, desc);

        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(customListview);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            }
        });
    }

}