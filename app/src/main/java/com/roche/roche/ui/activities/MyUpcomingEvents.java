package com.roche.roche.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.roche.roche.R;
import com.roche.roche.adapters.EventListingAdapter;
import com.roche.roche.base.PageHeader;

public class MyUpcomingEvents extends PageHeader {

    private ListView listView;
    private String names[] = {
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd"

    };

    private String desc[] = {
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd"

    };

    @Override
    public int getLayout() {
        return R.layout.activity_my_upcoming_events;
    }

    @Override
    protected String getActionBarTitle() {
        return "Upcoming Events";
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);

        EventListingAdapter customListview = new EventListingAdapter(this, names, desc);

        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(customListview);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //Toast.makeText(getApplicationContext(),"You Clicked "+names[i], Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MyUpcomingEvents.this, MessageDetails.class);
                startActivity(intent);
            }
        });
    }
}
