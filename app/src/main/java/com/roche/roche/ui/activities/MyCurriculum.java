package com.roche.roche.ui.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.github.lzyzsd.circleprogress.DonutProgress;
import com.roche.roche.R;
import com.roche.roche.base.PageHeader;
import com.roche.roche.ui.fragments.TabFragment;

import java.util.Timer;
import java.util.TimerTask;

public class MyCurriculum extends PageHeader {

    private Timer timer;
    private DonutProgress donutProgress;

    private FragmentManager mFragmentManager;
    private FragmentTransaction mFragmentTransaction;

    @Override
    public int getLayout() {
        return R.layout.activity_my_curriculum;
    }

    @Override
    protected String getActionBarTitle() {
        return "My Curriculum";
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);

        donutProgress = (DonutProgress) findViewById(R.id.circularProgressbar);

        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        donutProgress.setProgress(donutProgress.getProgress() + 1);

                    }

                });
            }
        }, 1000,100);

        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.containerView, new TabFragment()).commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
    }
}
