package com.roche.roche.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.roche.roche.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void a(View view){
        Intent intent = new Intent(this,AssessmentCongratulations.class);
        startActivity(intent);
    }

    public void b(View view){
        Intent intent = new Intent(this,AssessmentSorry.class);
        startActivity(intent);
    }
    public void c(View view){
        Intent intent = new Intent(this,AddExtraAttendees.class);
        startActivity(intent);
    }
    public void d(View view){
        Intent intent = new Intent(this,AttendanceCheck.class);
        startActivity(intent);
    }
    public void e(View view){
        Intent intent = new Intent(this,Attendance.class);
        startActivity(intent);
    } public void f(View view){
        Intent intent = new Intent(this,AttendanceList.class);
        startActivity(intent);
    }
    public void g(View view){
        Intent intent = new Intent(this,CourseDetails.class);
        startActivity(intent);
    }
    public void h(View view){
        Intent intent = new Intent(this,CourseDetails01.class);
        startActivity(intent);
    } public void i(View view){
        Intent intent = new Intent(this,CourseDetails03.class);
        startActivity(intent);
    } public void j(View view){
        Intent intent = new Intent(this,CoursesInProgress.class);
        startActivity(intent);
    } public void k(View view){
        Intent intent = new Intent(this,MyUpcomingEvents.class);
        startActivity(intent);
    } public void l(View view){
        Intent intent = new Intent(this,AcademyCourses.class);
        startActivity(intent);
    }
    public void m(View view){
        Intent intent = new Intent(this,Filters.class);
        startActivity(intent);
    } public void n(View view){
        Intent intent = new Intent(this,HomeScreen.class);
        startActivity(intent);
    } public void o(View view){
        Intent intent = new Intent(this,InviteNewMember.class);
        startActivity(intent);
    } public void p(View view){
        Intent intent = new Intent(this,Messages.class);
        startActivity(intent);
    } public void q(View view){
        Intent intent = new Intent(this,MessageDetails.class);
        startActivity(intent);
    } public void r(View view){
        Intent intent = new Intent(this,MyCertificates.class);
        startActivity(intent);
    } public void s(View view){
        Intent intent = new Intent(this,MyCertificates.class);
        startActivity(intent);
    } public void t(View view){
        Intent intent = new Intent(this,MyProfile.class);
        startActivity(intent);
    } public void u(View view){
        Intent intent = new Intent(this,MyTeam.class);
        startActivity(intent);
    } public void v(View view){
        Intent intent = new Intent(this,Notification.class);
        startActivity(intent);
    } public void w(View view){
        Intent intent = new Intent(this,OnlineCourseDetails.class);
        startActivity(intent);
    } public void x(View view){
        Intent intent = new Intent(this,OnlineCourseDetails01.class);
        startActivity(intent);
    } public void y(View view){
        Intent intent = new Intent(this,PostNewMessage.class);
        startActivity(intent);
    } public void z(View view) {
        Intent intent = new Intent(this, PreCourseSurvey.class);
        startActivity(intent);

    }

    public void zz(View view){
        Intent intent = new Intent(this,PrivacyPolicy.class);
        startActivity(intent);
    }
    public void ww(View view) {
        Intent intent = new Intent(this, Register.class);
        startActivity(intent);
    }


    public void uu(View view) {
        Intent intent = new Intent(this, SignIn.class);
        startActivity(intent);
    }
    public void cc(View view) {
        Intent intent = new Intent(this, SuggestCourse.class);
        startActivity(intent);
    }
    public void tt(View view) {
        Intent intent = new Intent(this, TermsConditions.class);
        startActivity(intent);
    }

    public void tl(View view) {
        Intent intent = new Intent(this, Tutorial.class);
        startActivity(intent);
    }

    public void up(View view) {
        Intent intent = new Intent(this, UpdatePassword.class);
        startActivity(intent);
    }

    public void wc(View view) {
        Intent intent = new Intent(this, Welcome.class);
        startActivity(intent);
    }













}
