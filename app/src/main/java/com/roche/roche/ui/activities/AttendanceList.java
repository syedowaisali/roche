package com.roche.roche.ui.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.roche.roche.R;
import com.roche.roche.adapters.AttendanceLocationAdapter;
import com.roche.roche.base.PageHeader;

public class AttendanceList extends PageHeader {

    private ListView listView;
    private String names[] = {
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd"

    };

    private String desc[] = {
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd"

    };

    @Override
    public int getLayout() {
        return R.layout.activity_attendance_list;
    }

    @Override
    protected String getActionBarTitle() {
        return "Attendance";
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);

        AttendanceLocationAdapter customListview = new AttendanceLocationAdapter(this, names, desc);

        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(customListview);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                gotoActivity(MessageDetails.class);
            }
        });
    }
}
