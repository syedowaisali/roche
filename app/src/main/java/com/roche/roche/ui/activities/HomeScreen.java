package com.roche.roche.ui.activities;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.flyco.pageindicator.indicator.base.PageIndicator;
import com.roche.roche.R;
import com.roche.roche.adapters.HomeUpComingEventsAdapter;
import com.roche.roche.base.Drawer;

public class HomeScreen extends Drawer {

    int pStatus = 0;
    private Handler handler = new Handler();
    TextView tv;
    HomeUpComingEventsAdapter mAdapter;
    ViewPager mPager;
    PageIndicator mIndicator;

    @Override
    public int getLayout() {
        return R.layout.activity_home_screen;
    }

    @Override
    protected String getActionBarTitle() {
        return "Welcome";
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);

        Resources res = getResources();
        Drawable drawable = res.getDrawable(R.drawable.home_pro_load);
        final ProgressBar mProgress = (ProgressBar) findViewById(R.id.circularProgressbar);
        mProgress.setProgress(0);   // Main Progress
        mProgress.setSecondaryProgress(100); // Secondary Progress
        mProgress.setMax(100); // Maximum Progress
        mProgress.setProgressDrawable(drawable);

        tv = (TextView) findViewById(R.id.tv);
        new Thread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                while (pStatus < 100) {
                    pStatus += 1;

                    handler.post(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            mProgress.setProgress(pStatus);
                            tv.setText(pStatus + "%");

                        }
                    });
                    try {
                        // Sleep for 200 milliseconds.
                        // Just to display the progress slowly
                        Thread.sleep(200); //thread will take approx 1.5 seconds to finish
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();


        mAdapter = new HomeUpComingEventsAdapter(getSupportFragmentManager());
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);

        mIndicator = (PageIndicator) findViewById(R.id.indicator);
        mIndicator.setViewPager(mPager);
    }


}
