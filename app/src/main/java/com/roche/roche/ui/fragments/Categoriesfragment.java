package com.roche.roche.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.roche.roche.R;
import com.roche.roche.adapters.categoriesAdapter;
import com.roche.roche.models.categories;

import java.util.ArrayList;



/**
 * Created by Ratan on 7/29/2015.
 */
public class Categoriesfragment extends Fragment {

    ListView gv;
    Context context;
    ArrayList prgmName;
    public static String [] prgmNameList={"Let Us C","c++","JAVA","Jsp","Microsoft .Net","Android","PHP","Jquery","JavaScript"};
    public static int [] prgmImages= { R.drawable.ic_launcher,
                                       R.drawable.ic_launcher,
                                       R.drawable.ic_launcher,
                                       R.drawable.ic_launcher,
                                       R.drawable.ic_launcher,
                                       R.drawable.ic_launcher,
                                       R.drawable.ic_launcher,
                                       R.drawable.ic_launcher,
                                       R.drawable.ic_launcher
    };




    ArrayList<categories> catlist;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootview = inflater.inflate(R.layout.primary_layout,null);


        Init(rootview);
        Toast.makeText(getActivity(), "Fragment categories", Toast.LENGTH_SHORT).show();

        return rootview;

    }
    public void Init(View v){

        gv=(ListView) v.findViewById(R.id.gridView1);
        catlist = new ArrayList<categories>();

        for(int i=0;i<prgmNameList.length;i++) {
            catlist.add(new categories(prgmImages[i],prgmNameList[i]));
        }
        categoriesAdapter adpt = new categoriesAdapter(getActivity(),catlist);
        gv.setAdapter(adpt);
    }
}
