package com.roche.roche.ui.activities;

import com.roche.roche.R;
import com.roche.roche.base.PageHeader;

public class SeminarDetails extends PageHeader {

    @Override
    public int getLayout() {
        return R.layout.activity_seminar_details;
    }

    @Override
    protected String getActionBarTitle() {
        return "Seminar Detail";
    }
}
