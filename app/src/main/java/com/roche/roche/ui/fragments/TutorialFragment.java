package com.roche.roche.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.roche.roche.R;

/**
 * Created by hshaikh on 6/26/2016.
 */
public class TutorialFragment extends Fragment {

    int position;

    Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
    }

    public Fragment newInstance(int _positon) {
        TutorialFragment tutFrag = new TutorialFragment();
        tutFrag.position = _positon;
        return tutFrag;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        View v = inflater.inflate(R.layout.tutorial_layout, container, false);

        //    ImageView imgNumber = (ImageView) v.findViewById(R.id.imgNumber);
        ImageView imgMain = (ImageView) v.findViewById(R.id.imgMainImage);
        //  TextView txtMainHead = (TextView) v.findViewById(R.id.txtHeading);
        TextView txtDesc = (TextView) v.findViewById(R.id.txtDescription);

        String textWithSubHead = "";

        if (position == 0) {

            //   imgNumber.setImageResource(R.mipmap.ic_launcher);
            imgMain.setImageResource(R.drawable.tutoirl);
            //   txtMainHead.setText(R.string.first_tut_head);
            txtDesc.setText(R.string.first_tut_desc);


        } else if (position == 1) {

            //  imgNumber.setImageResource(R.mipmap.ic_launcher);
            imgMain.setImageResource(R.drawable.tutoirl);
            // txtMainHead.setText(R.string.first_tut_head);
            txtDesc.setText(R.string.first_tut_desc);

        } else if (position == 2) {

            //   imgNumber.setImageResource(R.mipmap.ic_launcher);
            imgMain.setImageResource(R.drawable.tutoirl);
            //   txtMainHead.setText(R.string.first_tut_head);
            txtDesc.setText(R.string.first_tut_desc);

        } else if (position == 3) {

            // imgNumber.setImageResource(R.mipmap.ic_launcher);
            imgMain.setImageResource(R.drawable.tutoirl);
            //   txtMainHead.setText(R.string.first_tut_head);
            txtDesc.setText(R.string.first_tut_desc);
        } else if (position == 4) {

            // imgNumber.setImageResource(R.mipmap.ic_launcher);
            imgMain.setImageResource(R.drawable.tutoirl);
            //  txtMainHead.setText(R.string.first_tut_head);
            txtDesc.setText(R.string.first_tut_desc);

        } else {
            //  imgNumber.setImageResource(R.mipmap.ic_launcher);
            imgMain.setImageResource(R.drawable.tutoirl);
            // txtMainHead.setText(R.string.first_tut_head);
            txtDesc.setText(R.string.first_tut_desc);
        }

        return v;
    }
}