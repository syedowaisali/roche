package com.roche.roche.ui.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.roche.roche.R;
import com.roche.roche.adapters.NotificationAdapter;
import com.roche.roche.base.PageHeader;

public class Notification extends PageHeader {

    private ListView listView;
    private String names[] = {
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd"

    };

    private String desc[] = {
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd"

    };

    @Override
    public int getLayout() {
        return R.layout.activity_notification;
    }

    @Override
    protected String getActionBarTitle() {
        return "Notifications";
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);

        NotificationAdapter customListview = new NotificationAdapter(this, names, desc);

        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(customListview);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                gotoActivity(MessageDetails.class);
            }
        });
    }

}
