package com.roche.roche.ui.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.roche.roche.R;
import com.roche.roche.adapters.CourseListAdapter;
import com.roche.roche.base.PageHeader;

public class CoursesInProgress extends PageHeader {

    private ListView listView;
    private String names[] = {
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd"

    };

    private String desc[] = {
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd",
            "sdahdsahdasd"

    };


    private Integer imageid[] = {

    };

    @Override
    public int getLayout() {
        return R.layout.activity_courses_in_progress;
    }

    @Override
    protected String getActionBarTitle() {
        return "Courses In Progress";
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);

        CourseListAdapter customList = new CourseListAdapter(this, names, desc, imageid);

        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(customList);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //Toast.makeText(getApplicationContext(),"You Clicked "+names[i], Toast.LENGTH_SHORT).show();
            }
        });
    }
}
