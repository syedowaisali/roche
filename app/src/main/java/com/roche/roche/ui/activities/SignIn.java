package com.roche.roche.ui.activities;

import android.os.Bundle;
import android.view.View;

import com.roche.roche.R;
import com.roche.roche.base.Roche;

public class SignIn extends Roche {

    @Override
    public int getLayout() {
        return R.layout.activity_sign_in;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);

        // attach click listeners
        attachClickListener(
                R.id.btn_sign_in         ,
                R.id.btn_forgot_password ,
                R.id.btn_register        ,
                R.id.btn_privacy_policy  ,
                R.id.btn_travel_policy   ,
                R.id.btn_terms
        );
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch(v.getId()){
            case R.id.btn_sign_in         : signIn();                             break;
            case R.id.btn_forgot_password : gotoActivity(ForgotPassword.class);   break;
            case R.id.btn_register        : gotoActivity(Register.class);         break;
            case R.id.btn_privacy_policy  : gotoActivity(PrivacyPolicy.class);    break;
            case R.id.btn_travel_policy   : break;
            case R.id.btn_terms           : gotoActivity(TermsConditions.class);  break;
        }
    }

    private void signIn() {
        gotoActivity(HomeScreen.class, true);
    }
}


