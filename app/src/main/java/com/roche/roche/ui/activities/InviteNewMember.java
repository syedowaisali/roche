package com.roche.roche.ui.activities;

import com.roche.roche.R;
import com.roche.roche.base.PageHeader;

public class InviteNewMember extends PageHeader {

    @Override
    public int getLayout() {
        return R.layout.activity_invite_new_member;
    }

    @Override
    protected String getActionBarTitle() {
        return "Invite a New Member";
    }
}
