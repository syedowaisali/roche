package com.roche.roche.ui.activities;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.roche.roche.R;
import com.roche.roche.base.PageHeader;

import java.util.ArrayList;
import java.util.HashMap;

public class PreCourseSurvey extends PageHeader {

    private ProgressBar progressBar;
    private int progressStatus = 0;
    private TextView textView;
    private TextView tV;
    PreCourseSurvey m_this;
    private Handler handler = new Handler();

    @Override
    public int getLayout() {
        return R.layout.activity_pre_course_survey;
    }

    @Override
    protected String getActionBarTitle() {
        return "Pre Course Survey";
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);

        progressBar = (ProgressBar) findViewById(R.id.progressBar1);
        textView = (TextView) findViewById(R.id.textView1);
        tV = (TextView) findViewById(R.id.tV);
        // Start long running operation in a background thread
        new Thread(new Runnable() {
            public void run() {
                while (progressStatus < 5) {
                    progressStatus += 1;
                    // Update the progress bar and display the
                    //current value in the text view
                    handler.post(new Runnable() {
                        public void run() {
                            progressBar.setProgress(progressStatus);
                            textView.setText(progressStatus+"/"+"");
                        }
                    });
                    try {
                        // Sleep for 200 milliseconds.
                        //Just to display the progress slowly
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
        m_this = this;

        final ArrayList<HashMap<String, Object>> mData = new ArrayList<>();

        HashMap<String, Object> map1 = new HashMap<String, Object>();
        map1.put("maintext", "Go to school and get my degree");


        mData.add(map1);

        HashMap<String, Object> map2 = new HashMap<String, Object>();
        map2.put("maintext", "Make a lot of money working from home");// no small text of this item!

        mData.add(map2);

        HashMap<String, Object> map3 = new HashMap<String, Object>();
        map3.put("maintext", "I would love to find a new game to play");
        mData.add(map3);

               HashMap<String, Object> map34 = new HashMap<String, Object>();
        map34.put("maintext", "Earn extra money by taking surveys");
        mData.add(map34);

       HashMap<String, Object> map35 = new HashMap<String, Object>();
        map35.put("maintext", "Earn extra money by taking surveys");
        mData.add(map35);
        HashMap<String, Object> map36 = new HashMap<String, Object>();
        map36.put("maintext", "Earn extra money by taking surveys");
        mData.add(map36);
        HashMap<String, Object> map37 = new HashMap<String, Object>();
        map37.put("maintext", "Earn extra money by taking surveys3");
        mData.add(map37);
        HashMap<String, Object> map38 = new HashMap<String, Object>();
        map38.put("maintext", "Earn extra money by taking surveys");
        mData.add(map38);

        HashMap<String, Object> map39 = new HashMap<String, Object>();
        map39.put("maintext", "Earn extra money by taking surveys");
        mData.add(map39);


        HashMap<String, Object> map395 = new HashMap<String, Object>();
        map395.put("maintext", "Earn extra money by taking surveys");
        mData.add(map395);
        HashMap<String, Object> map31 = new HashMap<String, Object>();
        map31.put("maintext", "Earn extra money by taking surveys");
        mData.add(map31);

       /* HashMap<String, Object> map40 = new HashMap<String, Object>();
        map40.put("maintext", "Large text 3");
        mData.add(map40);
        HashMap<String, Object> map41 = new HashMap<String, Object>();
        map41.put("maintext", "Large text 3");
        mData.add(map41);
        HashMap<String, Object> map42 = new HashMap<String, Object>();
        map42.put("maintext", "Large text 3");
        mData.add(map42);
        HashMap<String, Object> map43 = new HashMap<String, Object>();
        map43.put("maintext", "Large text 3");
        mData.add(map43);
        HashMap<String, Object> map45 = new HashMap<String, Object>();
        map45.put("maintext", "Large text 3");
        mData.add(map45);



        HashMap<String, Object> map49 = new HashMap<String, Object>();
        map49.put("maintext", "Large text 3");
        mData.add(map49);
        HashMap<String, Object> map48 = new HashMap<String, Object>();
        map48.put("maintext", "Large text 3");
        mData.add(map48);
        HashMap<String, Object> map47 = new HashMap<String, Object>();
        map47.put("maintext", "Large text 3");
        mData.add(map47);
        HashMap<String, Object> map44 = new HashMap<String, Object>();
        map44.put("maintext", "Large text 3");
        mData.add(map44);
        HashMap<String, Object> map454 = new HashMap<String, Object>();
        map454.put("maintext", "Large text 3");
        mData.add(map454);*/





        for (HashMap<String, Object> m :mData) //make data of this view should not be null (hide )
            m.put("checked", false);
        //end init data


        final ListView lv = (ListView) m_this.findViewById(R.id.lv1);
        lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);


        final SimpleAdapter adapter = new SimpleAdapter(m_this, mData,
                R.layout.pre_coures_layout,
                new String[] {"maintext", "checked"},
                new int[] {R.id.tv_MainText, R.id.rb_Choice});

        adapter.setViewBinder(new SimpleAdapter.ViewBinder()
        {
            public boolean setViewValue(View view, Object data, String textRepresentation)
            {
                if (data == null) //if 2nd line text is null, its textview should be hidden
                {
                    view.setVisibility(View.GONE);
                    return true;
                }
                view.setVisibility(View.VISIBLE);
                return false;
            }

        });


        // Bind to our new adapter.
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int arg2, long arg3) {
                RadioButton rb = (RadioButton) v.findViewById(R.id.rb_Choice);
                if (!rb.isChecked()) //OFF->ON
                {
                    for (HashMap<String, Object> m :mData) //clean previous selected
                        m.put("checked", false);

                    mData.get(arg2).put("checked", true);
                    adapter.notifyDataSetChanged();
                }
            }
        });

        //show result
     /*   ((Button)m_this.findViewById(R.id.Button01)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int r = -1;
                for (int i = 0; i < m_data.size(); i++) //clean previous selected
                {
                    HashMap<String, Object> m = m_data.get(i);
                    Boolean x = (Boolean) m.get("checked");
                    if (x == true)
                    {
                        r = i;
                        break; //break, since it's a single choice list
                    }
                }
                new AlertDialog.Builder(m_this).setMessage("you selected:"+r).show();
            }
        });*/
    }
}
