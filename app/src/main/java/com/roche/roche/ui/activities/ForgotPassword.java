package com.roche.roche.ui.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.roche.roche.R;
import com.roche.roche.base.PageHeader;

public class ForgotPassword extends PageHeader {

    @Override
    public int getLayout() {
        return R.layout.activity_forgot_password;
    }

    @Override
    protected String getActionBarTitle() {
        return "Forgot Password";
    }
}
