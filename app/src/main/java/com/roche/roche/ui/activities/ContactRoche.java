package com.roche.roche.ui.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.roche.roche.R;
import com.roche.roche.base.PageHeader;
import com.roche.roche.ui.fragments.ContactTabFragment;

public class ContactRoche extends PageHeader {

    FragmentManager mFragmentManager;

    FragmentTransaction mFragmentTransaction;

    @Override
    public int getLayout() {
        return R.layout.activity_contact_roche;
    }

    @Override
    protected String getActionBarTitle() {
        return "Contact Roche";
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);

        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.container,new ContactTabFragment()).commit();

    }






}
