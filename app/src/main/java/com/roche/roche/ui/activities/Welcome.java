package com.roche.roche.ui.activities;

import android.os.Bundle;

import com.roche.roche.R;
import com.roche.roche.base.PageHeader;


public class Welcome extends PageHeader {

    @Override
    public int getLayout() {
        return R.layout.activity_welcome;
    }

    @Override
    protected String getActionBarTitle() {
        return "Welcome";
    }

    @Override
    protected boolean isVisibleBackIcon() {
        return false;
    }

}
