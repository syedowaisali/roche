package com.roche.roche.ui.activities;

import android.os.Bundle;

import com.roche.roche.R;
import com.roche.roche.base.PageHeader;

public class AttendanceCheck extends PageHeader {

    @Override
    public int getLayout() {
        return R.layout.activity_attendance_check;
    }

    @Override
    protected String getActionBarTitle() {
        return "Attendance Check";
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);
    }
}
