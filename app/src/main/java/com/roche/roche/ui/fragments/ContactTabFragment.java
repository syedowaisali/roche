package com.roche.roche.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.roche.roche.R;

/**
 * Created by hshaikh on 7/18/2016.
 */
public class ContactTabFragment extends Fragment {

    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    public static int int_items = 2 ;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle  savedInstanceState) {
        /**
         *Inflate tab_layout and setup Views.
         */
        View x =  inflater.inflate(R.layout.contact_tab_layout,null);
        tabLayout = (TabLayout) x.findViewById(R.id.tab);
        viewPager = (ViewPager) x.findViewById(R.id.viewpagers);




        /**
         *Set an Apater for the View Pager
         */
        viewPager.setAdapter(new MyAdapter(getChildFragmentManager()));

        /**
         * Now , this is a workaround ,
         * The setupWithViewPager dose't works without the runnable .
         * Maybe a Support Library Bug .
         */

        tabLayout.post(new Runnable() {
            @Override
            public void run() {
               tabLayout.setupWithViewPager(viewPager);

              tabLayout.getTabAt(0).setCustomView(R.layout.tab_con_contact);

                tabLayout.getTabAt(1).setCustomView(R.layout.tab_con_enqiry);

                TextView txt = (TextView) tabLayout.getTabAt(0).getCustomView().findViewById(R.id.txt);
               // TextView txt2 = (TextView) tabLayout.getTabAt(1).getCustomView().findViewById(R.id.txt2);
               /* if(tabLayout.getTabAt(0).isSelected())
                {
                    txt2.setTextColor(Color.BLUE);

                }*/

               /* if(tabLayout.getTabAt(1).isSelected())
                {
                    txt.setTextColor(Color.BLUE);
                }*/
            }
        });

        return x;

    }

    class MyAdapter extends FragmentPagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * Return fragment with respect to Position .
         */

        @Override
        public Fragment getItem(int position)
        {
            switch (position){
                case 0 : return new Contactfragment();
                case 1 : return new GeneralEnquiryFragment();
                // case 2 : return new UpdatesFragment();
            }
            return null;
        }

        @Override
        public int getCount() {

            return int_items;

        }

        /**
         * This method returns the title of the tab according to the position.
         */

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position){
                case 0 :
                    return "Contact Details";
                case 1 :
                    return "General Enquiry";
//                case 2 :
//                    return "Updates";
            }
            return null;
        }



    }

}