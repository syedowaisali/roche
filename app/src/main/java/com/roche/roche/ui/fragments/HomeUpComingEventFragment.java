package com.roche.roche.ui.fragments;

import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.TextView;

import com.crystal.base.BaseFragment;
import com.roche.roche.R;
import com.roche.roche.ui.activities.SeminarDetails;

/**
 * Created by hshaikh on 7/11/2016.
 */
public class HomeUpComingEventFragment extends BaseFragment{

    private int position;

    @Override
    public int getLayout() {
        return R.layout.home_up_coming_event;
    }

    @Override
    public void init() {
        super.init();
        TextView ClinicalChemistry = (TextView) rootView.findViewById(R.id.ClinicalChemistry);
        TextView ThisCourse = (TextView) rootView.findViewById(R.id.ThisCourse);
        TextView date = (TextView) rootView.findViewById(R.id.date);
        TextView location = (TextView) rootView.findViewById(R.id.location);

        for(int a = 0; a<10; a++){
            ClinicalChemistry.setText(R.string.clinical_chemistry_amp_immunology_troubleshooting_training);
            ThisCourse.setText(R.string.this_course_provides_clear_explanation_of_basic_technology_and_troubleshooting_techniques);
            date.setText(R.string.thur_28th_feb_8pm);
            location.setText(R.string.media_one_hotel_media_city_dubai_uae);
        }

        // attach click listener
        attachClickListener(R.id.btn_goto_event_detail);
    }

    public Fragment newInstance(int _positon) {
        HomeUpComingEventFragment tutFrag = new HomeUpComingEventFragment();
        tutFrag.position = _positon;
        return tutFrag;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.btn_goto_event_detail: gotoActivity(SeminarDetails.class); break;
        }
    }
}