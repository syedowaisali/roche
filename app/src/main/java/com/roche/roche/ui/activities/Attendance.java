package com.roche.roche.ui.activities;

import android.os.Bundle;
import android.view.View;

import com.roche.roche.R;
import com.roche.roche.base.PageHeader;

public class Attendance extends PageHeader {

    @Override
    public int getLayout() {
        return R.layout.activity_attendance;
    }

    @Override
    protected String getActionBarTitle() {
        return "Attendance";
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);

        // attach click listener
        attachClickListener(R.id.btn_check_attendance, R.id.btn_scan_qr_code, R.id.btn_add_extra_attendees);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.btn_check_attendance: gotoActivity(AttendanceCheck.class); break;
            case R.id.btn_scan_qr_code: gotoActivity(BarcodeScanner.class); break;
            case R.id.btn_add_extra_attendees: gotoActivity(AddExtraAttendees.class); break;
        }
    }

}
