package com.roche.roche.ui.activities;

import android.os.Bundle;

import com.roche.roche.R;
import com.roche.roche.base.PageHeader;

public class AssessmentSorry extends PageHeader {

    @Override
    public int getLayout() {
        return R.layout.activity_assessment_sorry;
    }

    @Override
    protected String getActionBarTitle() {
        return "Assessment";
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);
    }
}
