package com.roche.roche.ui.activities;

import android.os.Bundle;
import android.view.View;

import com.roche.roche.R;
import com.roche.roche.base.FilterHeader;


public class Filters extends FilterHeader {

    @Override
    public int getLayout() {
        return R.layout.activity_filters;
    }

    @Override
    protected String getActionBarTitle() {
        return "Filters";
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);

        // attach click listeners
        attachClickListener(R.id.btn_apply_filters);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.btn_apply_filters: applyFilters(); break;
        }
    }

    private void applyFilters(){

    }
}
