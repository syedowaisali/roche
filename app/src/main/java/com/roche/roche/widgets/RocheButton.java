package com.roche.roche.widgets;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;

import com.crystal.utilities.Api;
import com.crystal.widgets.CTLIconButton;
import com.roche.roche.R;

/**
 * Created by hshaikh on 6/30/2016.
 */
public class RocheButton extends CTLIconButton {

    //////////////////////////////////////////
    // PRIVATE VAR
    //////////////////////////////////////////

    private Drawable backgroundDrawable;
    private ColorStateList textColor;
    private float textSize;
    private boolean textAllCaps;
    private String typeface;

    //////////////////////////////////////////
    // CONSTRUCTOR
    //////////////////////////////////////////

    public RocheButton(Context context) {
        this(context, null);
    }

    public RocheButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RocheButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        // prevent exception in Android Studio / ADT interface builder
        if(isInEditMode()) return;

        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.RocheButton);
        try{
            backgroundDrawable = array.getDrawable(R.styleable.RocheButton_b_background);
            textColor          = array.getColorStateList(R.styleable.RocheButton_b_textColor);
            textSize           = array.getDimension(R.styleable.RocheButton_b_textSize, getContext().getResources().getDimension(R.dimen._5sdp));
            textAllCaps        = array.getBoolean(R.styleable.RocheButton_b_textAllCaps, false);
            typeface           = array.getString(R.styleable.RocheButton_b_Typeface);
        }
        finally {
            array.recycle();
        }

        init();
    }

    //////////////////////////////////////////
    // INITIALIZING
    //////////////////////////////////////////

    private void init(){
        if(backgroundDrawable == null) backgroundDrawable = ContextCompat.getDrawable(getContext(), R.drawable.button_hover);
        if(textColor == null)          textColor          = ContextCompat.getColorStateList(getContext(), R.color.white);
        if(typeface == null)           typeface = "fonts/tahomabd.ttf";

        setBackground(backgroundDrawable);
        setTextColor(textColor);
        setTextSize(textSize);
        setAllCaps(textAllCaps);
        setTextAlignment(TEXT_ALIGNMENT_CENTER);
        setGravity(Gravity.CENTER);
        setFontface(typeface);
        setClickable(true);
    }

    //////////////////////////////////////////
    // PRIVATE METHODS
    //////////////////////////////////////////

    //////////////////////////////////////////
    // PUBLIC METHODS
    //////////////////////////////////////////

    public void setFontface(String typeface){
        if(typeface != null){
            try {
                setTypeface(Typeface.createFromAsset(getContext().getAssets(), typeface));
            }
            catch (Exception ex){
                Log.e(Api.TAG, ex.getMessage());
            }
        }
    }


}
