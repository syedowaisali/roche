package com.roche.roche.widgets;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;

import com.crystal.widgets.CTLTextView;
import com.roche.roche.R;

/**
 * Created by hshaikh on 6/30/2016.
 */
public class RocheTextView extends CTLTextView {

    private ColorStateList textColor;

    public RocheTextView(Context context) {
        this(context, null);
    }

    public RocheTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RocheTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        // prevent exception in Android Studio / ADT interface builder
        if(isInEditMode()) return;

        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.RocheTextView);
        try{
            textColor = array.getColorStateList(R.styleable.RocheTextView_tv_textColor);
        }
        finally {
            array.recycle();
        }

        init();
    }

    private void init(){
       /* if(textColor == null) textColor = ContextCompat.getColorStateList(getContext(), R.color.color_113c8b);
        setTextColor(textColor);*/
    }
}
