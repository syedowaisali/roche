package com.roche.roche.widgets;

import android.content.Context;
import android.util.AttributeSet;

import com.crystal.widgets.CTLTextView;

/**
 * Created by owais.ali on 8/11/2016.
 */
public class UserEmailView extends CTLTextView {

    public UserEmailView(Context context) {
        super(context);
    }

    public UserEmailView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public UserEmailView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
