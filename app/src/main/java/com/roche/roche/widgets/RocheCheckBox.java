package com.roche.roche.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CheckBox;

/**
 * Created by hshaikh on 6/30/2016.
 */
public class RocheCheckBox extends CheckBox {

    public RocheCheckBox(Context context) {
        this(context, null);
    }

    public RocheCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RocheCheckBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
