package com.roche.roche.widgets;

import android.content.Context;
import android.util.AttributeSet;

import com.crystal.widgets.CTLEditText;

/**
 * Created by hshaikh on 6/30/2016.
 */
public class RocheEditText extends CTLEditText {



    public RocheEditText(Context context) {
        this(context, null);
    }

    public RocheEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RocheEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);


    }

}
