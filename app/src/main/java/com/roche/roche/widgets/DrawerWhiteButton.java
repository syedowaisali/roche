package com.roche.roche.widgets;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;

import com.roche.roche.R;
import com.roche.roche.base.DrawerButton;

/**
 * Created by owais.ali on 8/11/2016.
 */
public class DrawerWhiteButton extends DrawerButton {

    private Drawable drawableNormal;

    public DrawerWhiteButton(Context context) {
        super(context);
    }

    public DrawerWhiteButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DrawerWhiteButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void init() {
        super.init();
        Bitmap bitmap = addGradient(((BitmapDrawable) drawable).getBitmap());
        drawableNormal = new BitmapDrawable(getResources(), bitmap);
        setCompoundDrawablesWithIntrinsicBounds(drawableNormal, null, null, null);
    }

    @Override
    public int getTextColorNormal() {
        return ContextCompat.getColor(getContext(), R.color.color_113c8b);
    }

    @Override
    public int getTextColorHover() {
        return Color.WHITE;
    }

    @Override
    public int getBackgroundColorNormal() {
        return Color.WHITE;
    }

    @Override
    public int getBackgroundColorHover() {
        return ContextCompat.getColor(getContext(), R.color.color_113c8b);
    }

    @Override
    protected void touchDown(float x, float y) {
        super.touchDown(x, y);
        setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
    }

    @Override
    protected void touchUp(float x, float y) {
        super.touchUp(x, y);
        setCompoundDrawablesWithIntrinsicBounds(drawableNormal, null, null, null);
    }

    private Bitmap addGradient(Bitmap originalBitmap) {
        final int width = originalBitmap.getWidth();
        final int height = originalBitmap.getHeight();
        final Bitmap updatedBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(updatedBitmap);

        canvas.drawBitmap(originalBitmap, 0, 0, null);

        final Paint paint = new Paint();
        final LinearGradient shader = new LinearGradient(0, 0, 0, height, ContextCompat.getColor(getContext(), R.color.color_113c8b), ContextCompat.getColor(getContext(), R.color.color_3e8ada), Shader.TileMode.CLAMP);
        paint.setShader(shader);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawRect(0, 0, width, height, paint);

        return updatedBitmap;
    }
}
