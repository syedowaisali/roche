package com.roche.roche.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RadioButton;

/**
 * Created by hshaikh on 6/30/2016.
 */
public class RocheRadioButton extends RadioButton {

    public RocheRadioButton(Context context) {
        this(context, null);
    }

    public RocheRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RocheRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
