package com.roche.roche.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.roche.roche.R;

/**
 * Created by Syed Owais Ali on 8/14/2016.
 */
public class ColorImageView extends ImageView {

    private static final short NO_COLOR = -1;

    private int imageColor;

    public ColorImageView(Context context) {
        this(context, null);
    }

    public ColorImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ColorImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        final TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.ColorImageView);
        try {
            imageColor = array.getColor(R.styleable.ColorImageView_new_color, NO_COLOR);
        }
        finally {
            array.recycle();
        }

        init();
    }

    private void init(){
        if(imageColor != NO_COLOR){
            setColor(imageColor);
        }
    }

    public void setColor(final int color){
        setColorFilter(color);
    }
}
