package com.roche.roche.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;

import com.roche.roche.R;
import com.roche.roche.base.DrawerButton;

/**
 * Created by owais.ali on 8/11/2016.
 */
public class DrawerBlueButton extends DrawerButton {

    public DrawerBlueButton(Context context) {
        super(context);
    }

    public DrawerBlueButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DrawerBlueButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public int getTextColor(TypedArray typedArray) {
        return Color.WHITE;
    }

    @Override
    public int getTextColorHover(TypedArray typedArray) {
        return Color.WHITE;
    }

    @Override
    public int getBackgroundColor(TypedArray typedArray) {
        return ContextCompat.getColor(getContext(), R.color.color_113c8b);
    }

    @Override
    public int getBackgroundColorHover(TypedArray typedArray) {
        return ContextCompat.getColor(getContext(), R.color.color_3862ae);
    }

}
