package com.roche.roche.models;

/**
 * Created by User1 on 10-Feb-16.
 */
public class categories {

    private int image;
    private String categoryname;

    public categories(int image, String categoryname) {
        this.image = image;
        this.categoryname = categoryname;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }


}
