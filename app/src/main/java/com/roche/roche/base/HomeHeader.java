package com.roche.roche.base;

/**
 * Created by owais.ali on 8/11/2016.
 */
public abstract class HomeHeader extends Header {

    @Override
    protected boolean isVisibleDrawerIcon() {
        return true;
    }
}
