package com.roche.roche.base;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.View;

import com.roche.roche.R;
import com.roche.roche.ui.activities.AcademyCourses;
import com.roche.roche.ui.activities.ContactRoche;
import com.roche.roche.ui.activities.CoursesInProgress;
import com.roche.roche.ui.activities.InviteNewMember;
import com.roche.roche.ui.activities.Messages;
import com.roche.roche.ui.activities.MyBookmarks;
import com.roche.roche.ui.activities.MyCertificates;
import com.roche.roche.ui.activities.MyCurriculum;
import com.roche.roche.ui.activities.MyProfile;
import com.roche.roche.ui.activities.MyTeam;
import com.roche.roche.ui.activities.MyUpcomingEvents;
import com.roche.roche.ui.activities.SignIn;
import com.roche.roche.ui.activities.SuggestedCourses;

/**
 * Created by Syed Owais Ali on 8/13/2016.
 */
public abstract class Drawer extends HomeHeader{

    private DrawerLayout drawerLayout;

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);

        // get drawer reference from view
        drawerLayout = getView(R.id.drawer_layout);

        // attach click listeners
        attachClickListener(
                R.id.drawer_close_icon             ,    // close drawer
                R.id.drawer_btn_profile_settings   ,    // goto my profile activity
                R.id.drawer_btn_logout             ,    // logout app
                R.id.drawer_btn_invite_new_member  ,    // goto invite new member activity
                R.id.drawer_btn_home               ,    // goto home activity
                R.id.drawer_btn_academy_course     ,    // goto academy course activity
                R.id.drawer_btn_upcoming_events    ,    // goto upcoming events activity
                R.id.drawer_btn_my_curriculum      ,    // goto my curriculum activity
                R.id.drawer_btn_my_team            ,    // goto my team activity
                R.id.drawer_btn_course_in_progress ,    // goto course in progress activity
                R.id.drawer_btn_message            ,    // goto message activity
                R.id.drawer_btn_my_certificates    ,    // goto my certificates activity
                R.id.drawer_btn_my_bookmarks       ,    // goto my bookmarks activity
                R.id.drawer_btn_suggested_course   ,    // goto suggested course activity
                R.id.drawer_btn_contact_roche           // goto roche contact activity
        );
    }

    @Override
    protected void clickDrawerIcon(View v) {
        super.clickDrawerIcon(v);
        drawerLayout.openDrawer(GravityCompat.START);
    }

    @Override
    public void onClick(final View v) {
        super.onClick(v);

        // also close drawer if button is not equal to drawer open button id
        if(v.getId() != R.id.btn_drawer_icon) drawerLayout.closeDrawer(GravityCompat.START);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                switch (v.getId()){
                    case R.id.drawer_close_icon: drawerLayout.closeDrawer(GravityCompat.START); break;
                    case R.id.drawer_btn_profile_settings: gotoActivity(MyProfile.class); break;
                    case R.id.drawer_btn_logout: logout(); break;
                    case R.id.drawer_btn_invite_new_member: gotoActivity(InviteNewMember.class); break;
                    case R.id.drawer_btn_home: break;
                    case R.id.drawer_btn_academy_course: gotoActivity(AcademyCourses.class); break;
                    case R.id.drawer_btn_upcoming_events: gotoActivity(MyUpcomingEvents.class); break;
                    case R.id.drawer_btn_my_curriculum: gotoActivity(MyCurriculum.class); break;
                    case R.id.drawer_btn_my_team: gotoActivity(MyTeam.class); break;
                    case R.id.drawer_btn_course_in_progress: gotoActivity(CoursesInProgress.class); break;
                    case R.id.drawer_btn_message: gotoActivity(Messages.class); break;
                    case R.id.drawer_btn_my_certificates: gotoActivity(MyCertificates.class); break;
                    case R.id.drawer_btn_my_bookmarks: gotoActivity(MyBookmarks.class); break;
                    case R.id.drawer_btn_suggested_course: gotoActivity(SuggestedCourses.class); break;
                    case R.id.drawer_btn_contact_roche: gotoActivity(ContactRoche.class); break;
                }
            }
        }, 200);
    }

    private void logout(){
        gotoActivity(SignIn.class, true);
    }
}
