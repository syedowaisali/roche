package com.roche.roche.base;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;

import com.roche.roche.R;
import com.roche.roche.widgets.ColorImageView;
import com.roche.roche.widgets.RocheTextView;

/**
 * Created by Syed Owais Ali on 8/14/2016.
 */
public abstract class FilterHeader extends PageHeader {

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);

        if(actionbarContainer != null){
            actionbarContainer.setBackgroundColor(Color.WHITE);
        }

        if(actionbarBackIcon != null){
            ((ColorImageView) actionbarBackIcon).setColor(ContextCompat.getColor(this, R.color.color_113c8b));
        }

        if(actionbarTitle != null){
            ((RocheTextView) actionbarTitle).setTextColor(ContextCompat.getColor(this, R.color.color_113c8b));
        }
    }

    @Override
    protected boolean isVisibleFilterIcon() {
        return true;
    }

    @Override
    protected boolean isVisibleResetButton() {
        return true;
    }
}
