package com.roche.roche.base;

/**
 * Created by owais.ali on 8/11/2016.
 */
public abstract class PageHeader extends Header {

    @Override
    protected boolean isVisibleBackIcon() {
        return true;
    }
}
