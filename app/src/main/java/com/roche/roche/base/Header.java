package com.roche.roche.base;

import android.os.Bundle;
import android.view.View;

import com.roche.roche.R;
import com.roche.roche.widgets.RocheTextView;

/**
 * Created by owais.ali on 8/11/2016.
 */
public abstract class Header extends Roche {

    protected View actionbarContainer;
    protected View actionbarDrawerIcon;
    protected View actionbarBackIcon;
    protected View actionbarNotifyIcon;
    protected View actionbarPlusIcon;
    protected View actionbarResetButton;
    protected View actionbarFilterIcon;
    protected View actionbarTitle;

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);

        actionbarContainer   = getView(R.id.actionbar_container);
        actionbarDrawerIcon  = getView(R.id.btn_drawer_icon);
        actionbarBackIcon    = getView(R.id.btn_actionbar_back);
        actionbarNotifyIcon  = getView(R.id.actionbar_notify_wrapper);
        actionbarPlusIcon    = getView(R.id.btn_actionbar_plus);
        actionbarResetButton = getView(R.id.btn_actionbar_reset);
        actionbarFilterIcon  = getView(R.id.icon_actionbar_filter);
        actionbarTitle       = getView(R.id.actionbar_title);

        if(actionbarDrawerIcon != null){
            actionbarDrawerIcon.setVisibility(isVisibleDrawerIcon() ? View.VISIBLE : View.GONE);
            actionbarDrawerIcon.setOnClickListener(this);
        }

        if(actionbarBackIcon != null){
            actionbarBackIcon.setVisibility(isVisibleBackIcon() ? View.VISIBLE : View.GONE);
            actionbarBackIcon.setOnClickListener(this);
        }

        if(actionbarNotifyIcon != null){
            actionbarNotifyIcon.setVisibility(isVisibleNotifyIcon() ? View.VISIBLE : View.GONE);
            actionbarNotifyIcon.setOnClickListener(this);
        }

        if(actionbarPlusIcon != null){
            actionbarPlusIcon.setVisibility(isVisiblePlusIcon() ? View.VISIBLE : View.GONE);
            actionbarPlusIcon.setOnClickListener(this);
        }

        if(actionbarResetButton != null){
            actionbarResetButton.setVisibility(isVisibleResetButton() ? View.VISIBLE : View.GONE);
            actionbarResetButton.setOnClickListener(this);
        }

        if(actionbarFilterIcon != null){
            actionbarFilterIcon.setVisibility(isVisibleFilterIcon() ? View.VISIBLE : View.GONE);
        }

        ((RocheTextView) actionbarTitle).setText(getActionBarTitle());
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.btn_drawer_icon     : clickDrawerIcon(v);  break;
            case R.id.btn_actionbar_back  : clickBackIcon(v);    break;
            case R.id.action_notify_icon  : clickNotifyIcon(v);  break;
            case R.id.btn_actionbar_plus  : clickPlusIcon(v);    break;
            case R.id.btn_actionbar_reset : clickResetButton(v); break;
        }
    }

    protected void clickDrawerIcon(final View v){}

    protected void clickBackIcon(final View v){
        super.onBackPressed();
    }

    protected void clickNotifyIcon(final View v){}

    protected void clickPlusIcon(final View v){}

    protected void clickResetButton(final View v){}

    protected boolean isVisibleDrawerIcon(){
        return false;
    }

    protected boolean isVisibleBackIcon(){
        return false;
    }

    protected boolean isVisibleNotifyIcon(){
        return false;
    }

    protected boolean isVisiblePlusIcon(){
        return false;
    }

    protected boolean isVisibleResetButton(){
        return false;
    }

    protected boolean isVisibleFilterIcon(){
        return false;
    }

    protected abstract String getActionBarTitle();
}
