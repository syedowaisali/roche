package com.roche.roche.base;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.view.GravityCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;

import com.crystal.crystalbutton.widget.CrystalButton;

/**
 * Created by owais.ali on 8/4/2016.
 */
public abstract class DrawerButton extends CrystalButton {

    protected Drawable drawable;

    public DrawerButton(Context context) {
        super(context);
        init();
    }

    public DrawerButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DrawerButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    protected void init(){
        drawable = getCompoundDrawables()[0];
        setPadding((int)getResources().getDimension(com.intuit.sdp.R.dimen._20sdp), 0, 0, 0);
        setCompoundDrawablePadding((int)getResources().getDimension(com.intuit.sdp.R.dimen._15sdp));
        setClickable(true);
        setTypeface(Typeface.createFromAsset(getResources().getAssets(), "fonts/tahoma.ttf"));
    }

    protected void touchDown(final float x, final float y){}

    protected void touchUp(final float x, final float y){}

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        setGravity(GravityCompat.START | Gravity.CENTER_VERTICAL);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(), (int)getResources().getDimension(com.intuit.sdp.R.dimen._40sdp));
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                touchDown(event.getX(), event.getY());
                break;

            case MotionEvent.ACTION_UP:
                touchUp(event.getX(), event.getY());
                break;

            case MotionEvent.ACTION_CANCEL:
                touchUp(event.getX(), event.getY());
                break;

        }
        return super.onTouchEvent(event);
    }

    @Override
    public int getStyle(TypedArray typedArray) {
        return Style.BUBBLE;
    }
}
